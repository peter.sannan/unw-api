<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Entry extends Model
{
    use HasFactory;

    protected $guarded = [];

    public static function boot()
    {
        parent::boot();
        static::created(function ($model) {
            $model->token = static::getRandomToken();
            $model->save();
        });
    }

    protected static function getRandomToken()
    {
        $token = Str::random(30);
        if (self::whereToken($token)->exists()) {
            static::getRandomToken();
        }
        return $token;
    }

    public function getRouteKeyName()
    {
        return 'token';
    }
}
