<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class TokenValid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->header('Authorization') !== 'c0299c9e794e59c061b5cc7f7a43db19') {
            return response()->json('token not valid!', 401);
        }

        return $next($request);
    }
}
