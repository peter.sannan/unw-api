<?php

use App\Http\Controllers\EntriesController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/entry', [EntriesController::class, 'store'])->middleware('token.available');
Route::get('/entry/{entry}', [EntriesController::class, 'show'])->middleware('token.available');
Route::get('/home', [HomeController::class, 'index'])->middleware('token.available');